import React, { Component } from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import './App.css';
import SwitchComponent from './components/switchcomponent/switchComponent'
import routes from './Routes/index'

class App extends Component {
  render() {
    return (
    <Router>
        <div>
          <div  className="todoheader">
            <h2>TODO APP</h2>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <ul className="navbar-nav mr-auto">
              <Link to={'/'} className="nav-link" exact> Todolist </Link>
              <Link to={'/Addtodo'} className="nav-link" exact>Addtodo</Link>
            </ul>
            </nav>
         
          </div>
          <SwitchComponent routes={routes}/>
        </div>
      </Router>
    );
  }
}

export default App;
