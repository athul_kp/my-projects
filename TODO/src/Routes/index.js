import Addtodo from '../Views/Addtask/Container/index';
import Tableindex from '../Views/dashboard/Container/index';
const routes=[
    {
        path:"/",
        exact:true,
        component:Tableindex
    },
    {
        path:"",
        component:Addtodo
    },
]
export default routes