import apiUrl from '../../../constants/index'
import {post} from '../../../utils/apiCallback'

export function postData (body) {
  // alert(JSON.stringify(body))
  return {
    type: 'ADD_ACTION',
    payload: post(`${apiUrl}`, body)
  }
}
