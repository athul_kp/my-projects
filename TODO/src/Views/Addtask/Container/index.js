import React from 'react'
import { withFormik } from 'formik'
import * as Yup from 'yup'
import { withStyles, Card, CardContent, TextField, Button } from '@material-ui/core'
import styles from './style'
import { postData } from '../Action/Index'
import { connect } from 'react-redux'

const form = props => {
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit
  } = props

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
            <TextField
              id='name'
              label='name'
              value={values.name}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.name ? errors.name : ''}
              error={touched.name && Boolean(errors.name)}
              margin='dense'
              variant='outlined'
              fullWidth
            />
            <TextField
              id='task'
              label='task'
              value={values.task}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.task ? errors.task : ''}
              error={touched.task && Boolean(errors.task)}
              margin='dense'
              variant='outlined'
              fullWidth
            />
          </CardContent>
          <Button type='submit' color='secondary' disabled={isSubmitting}>
              SUBMIT</Button>
        </Card>
      </form>
    </div>
  )
}

const Form = withFormik({
  mapPropsToValues: ({
    name,
    task
  }) => {
    return {
      name: name || '',
      task: task || ''
    }
  },

  validationSchema: Yup.object().shape({
    name: Yup.string().required('Required'),
    task: Yup.string().required('Required'),
  }),

  handleSubmit: (values, { props, setSubmitting, resetForm }) => {
    setSubmitting(false)
    resetForm()
    alert('Inserted')
    props.dispatch(postData(values))
  }
})(form)

export default connect()(withStyles(styles)(Form))