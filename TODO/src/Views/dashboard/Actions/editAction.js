import { putData, deleteData } from '../../../utils/apiCallback'
import apiUrl from '../../../constants/index'

export function putProd(id, body) {
  return function(dispatch){
    dispatch({type: "EDIT_PRODUCT"});
    putData(`${apiUrl}/${id}`,body)
    .then((res)=>{
      dispatch({type:"EDIT_PRODUCT_FULFILLED",payload: {res: res , id:id}})
    }).catch((err)=>{
      dispatch({type: "EDIT_PRODUCT", payload: err.message})
    })
  };
}

export function deleteProd (id) {
  return dispatch => {
    dispatch({type: 'DELETE_DATA'})
    deleteData(`${apiUrl}/${id}`)
    .then(res => {
      dispatch({type: 'DELETE_DATA_FULFILLED', payload: {res: res, id: id}})
    })
    .catch(err => {
      dispatch({type: 'DELETE_DATA_REJECETED', payload: err.message})
    })
  }
}


