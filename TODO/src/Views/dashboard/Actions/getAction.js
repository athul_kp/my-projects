import apiUrl from '../../../constants/index'
import {get} from '../../../utils/apiCallback'
export function getProducts () {
  return {
    type: 'FETCH_PRODUCT',
    payload: get(`${apiUrl}`)
  }
}

export function searchData (data) {
  return {
    type: 'SEARCH_FILTER_DATA',
    payload: get(`${apiUrl}?search=${data}`)
  }
}
