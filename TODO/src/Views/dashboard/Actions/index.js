import apiUrl from '../../../constants/index'
import {get} from '../../../utils/apiCallback'

export function getData () {
    return {
      type: 'FETCH_PRODUCT',
      payload: fetcheddata()
    }
  }
   
  async function fetcheddata () {
    const axiosConfig = {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      }
    }
    const res = await get(`${apiUrl}`, axiosConfig)
    return res
  }
