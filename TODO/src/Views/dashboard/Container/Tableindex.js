import React from 'react'
import Search from '../../../components/Search'
import Editform from './editindex';
export default function Tableindex(props){
    return(
      
        <div>
            <Search/>
            <table>
                <tbody>
                    <tr className="tablename">
                        <td> Name </td>
                        <td> Task </td> 
                        <td> Delete </td>   
                        <td> Edit </td>    
                    </tr>
                    {props.rows.map((task)=>(
                        <tr key={task.id} > 
                            <td>{task.name.length>10 ? task.name.substr(0,10)+'...' : task.name}</td>
                            <td>{task.task.length >10 ? task.task.substr(0,10)+'...' : task.task}</td>
                            <td> 
                            <button onClick={()=>props.rowDelete(task.id)}> 
                                Remove
                            </button>    
                            </td>
                        <td>
                            <Editform id={task.id} name={task.name} task={task.task}/>
                        </td>
                        </tr>
                        ))}
                </tbody>
            </table>
        </div>
    ); 
}
// <Editpopup onClick={() => props.clicked(task.id)} id={task.id} name={task.name} task={task.task}/>