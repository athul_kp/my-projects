import React, { useState } from 'react';
import Modal from '@material-ui/core/Modal';
import useStyles from '../../../components/editForm/style';
import {putProd} from '../Actions/editAction';
import { connect } from "react-redux";

const EditForm=(props)=> {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [name, setName] = useState(props.name);
  const [task, setTask] = useState(props.task);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleSubmit = () => {
    if((name==="")&&(task==="")){
      alert("Fill all fields")
    }else if(name===""){
     alert("Please fill the name  field") 
    }else if (task==="") {
      alert("Please fill the task field")
    }else{
      const tasks={name:name,task:task}
      props.dispatch(putProd(props.id,tasks))
      handleClose();
    }
  }
  return (
    <div>
      <button onClick={handleOpen} >Edit</button>
      <Modal
        aria-labelledby="simple-modal-name"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <div className={classes.paper}>
          <h2 id="simple-modal-name">Edit Your Task</h2>
          <textarea className={classes.textsty} id="name" label="name"  value={name} onChange={e => setName(e.target.value)}/><br /><br />
          <textarea className={classes.textsty} id="task" label="Task"  value={task} onChange={e => setTask(e.target.value)}/><br />
          <button className={classes.textsty}  onClick={() => handleSubmit()}>
            Update
        </button>
        </div>
      </Modal>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    done: state.done
  };
};
export default connect(mapStateToProps)(EditForm);