import React from "react";
import { connect } from "react-redux";
import {deleteProd} from '../Actions/editAction'
import {getData} from '../Actions/index'
import Tableindex from './Tableindex'

class TableData extends React.Component {
    state = {
        id:null
    }
    changeHandler = (id) => {
        console.log(id)
        this.setState({id:id})
    }
    componentDidMount() {
        this.props.dispatch(getData())
    }
    deleteHandler = (id) => {
        // alert(id)
        this.props.dispatch(deleteProd(id))
    }
    render() {
        const display = (this.props.done === true) ? 
        <Tableindex rows={this.props.tasks.data} rowDelete={this.deleteHandler} clicked={this.changeHandler}/>
             : "Loading..."    
    return <> {display} </>
    }
}
const mapStateToProps = state =>{
    return{
        tasks: state.tasks,
        done: state.done
    }
}
export default connect(mapStateToProps)(TableData);