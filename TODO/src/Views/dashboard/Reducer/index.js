const reducer = (state ={
  fetching : false,
    fetched :false,
    tasks:[],
    error:null,
    done: false,
    deleted:false,
    deleting:false
},action)=>{
    switch(action.type){
        case 'FETCH_PRODUCT_PENDING': 
            return { ...state, fetching: true }
          
          case 'FETCH_PRODUCT_REJECTED': 
            return { ...state, fetching: false, error: action.payload }
          
          case 'FETCH_PRODUCT_FULFILLED': 
            console.log("ACTION = " , JSON.stringify(action.payload))
            return {
              ...state,
              fetching: false,
              fetched: true,
              done: true,
              tasks: action.payload
            }
          case 'DELETE_PRODUCT-PENDING':
            return {...state,deleting:true}
          
          case 'DELETE_PRODUCT_REJECTED' :
            return {...state,deleting:false,error:action.payload}
          
          case 'DELETE_DATA_FULFILLED': 
              const task = { ...state.tasks }
              const index = state.tasks.data.findIndex(prod => prod.id === action.payload.id.toString())
              task.data.splice(index, 1)
              return {
                ...state,
                done: true,
                deleted: false,
                tasks: task
              }
              case 'EDIT_PRODUCT_FULFILLED': {
                const task = { ...state.tasks }
                const index = task.data.findIndex(task => task.id === action.payload.id.toString())
                task.data[index] = action.payload.res.data
                return {
                  ...state,
                  done:true,
                  fetched:false,
                  tasks: task
                }
              }
              case 'SEARCH_FILTER_DATA_FULFILLED': {
                return {
                  ...state,
                  tasks: action.payload
                }
              }
              default : return {...state}
        }
    }
export default reducer;