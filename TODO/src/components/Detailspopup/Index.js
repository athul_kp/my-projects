import React, { useState } from 'react';
import Modal from '@material-ui/core/Modal';
import useStyles from '../../../components/editForm/style';
import { connect } from "react-redux";

const Details=(props)=> {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [name, setName] = useState(props.name);
  const [task, setTask] = useState(props.task);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div>
      <button onClick={handleOpen} >Edit</button>
      <Modal
        aria-labelledby="simple-modal-name"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <div className={classes.paper}>
          <h2 id="simple-modal-name">Edit Your Task</h2>
          <textarea className={classes.textsty} id="name" label="name"  value={name} onChange={e => setName(e.target.value)}/><br /><br />
          <textarea className={classes.textsty} id="task" label="Task"  value={task} onChange={e => setTask(e.target.value)}/><br />
        </div>
      </Modal>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    done: state.done
  };
};
export default connect(mapStateToProps)(Details);