import React from 'react'
import {searchData} from '../../Views/dashboard/Actions/getAction'
import {connect} from 'react-redux'

const Search = (props) => {
    const filterFunction = (event) => {
        props.dispatch(searchData(event.target.value))
    }
    return(
        <div style={{textAlign:"right",marginRight:"10%"}}>  
            <input  type="text"
            placeholder="Search"
            onChange={(event) => filterFunction(event)}
            /> 
        </div>
    )
}

export default connect(null, null)(Search)