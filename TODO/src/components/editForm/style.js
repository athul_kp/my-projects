import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      marginTop: 100,
      marginLeft: 500,
      width: 300,
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(2, 4, 3),
      textAlign: "center"
    },
    textsty: {
      padding: 10
    }
  }),
);
export default useStyles;