import React from 'react'
import { Switch, Route } from 'react-router-dom'

export default function SwitchComponent (props) {
  return (
    <Switch>
      {
        props.routes.map((route, index) => (
          <Route key={index}
            path={route.path}
            exact={route.exact}
            component={route.component}/>
        ))
      }
    </Switch>
  )
}