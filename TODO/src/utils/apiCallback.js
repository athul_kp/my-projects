import axios from 'axios'

export function get (url, payload = {}, headers ={}) {
  // console.log("URL = " + url)
  return axios.get(url, payload, headers)
}

export function putData (url, payload = {}, headers={}) {
  return axios.put(url, payload, headers)
}

export function post (url, payload = {}, headers={}) {
  alert(JSON.stringify(payload))
  return axios.post(url, payload, headers)
}

export function deleteData (url, payload = {}, headers={}) {
  alert("delete")
  return axios.delete(url, payload, headers)
}
