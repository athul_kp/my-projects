import React, { Component } from 'react';
import './App.css';
import Validation from './Validation/Validation';
import Char from './Char/Char';

class App extends Component{
  state={
    userInput : ''
  }
  inputChangeHandler = (event) => {
    this.setState({userInput:event.target.value});
  }
  deleteCharHandler = (index) => {
    const text = this.state.userInput.split('');
    text.splice(index, 1);
    const updateText = text.join('');
    this.setState({userInput : updateText})
  }
  render(){
    const charList =this.state.userInput.split('').map((ch,index)=>
      {
        return (<Char 
        charector={ch}
        key={index}
        clicked ={()=>this.deleteCharHandler(index)}/>
        )
      }
    ) 
    return(
     
      <div className="App"> 
        <p> Creating the second assignment   </p> 
        <input type="text" onChange={this.inputChangeHandler} value={this.state.userInput}/>
        <Validation inputLength={this.state.userInput.length}/>
        {charList}
      </div>
    )
  }
}

export default App;
