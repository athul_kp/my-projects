import React from 'react';


const Validation = (props)=>{
    return(
        <div>
            {props.inputLength > 5 ? 'text is too large' : 'text is too short'}
        </div>
    );
}

export default Validation