import React from 'react';

const userInput = (props) => {
    const styleButton ={
        border : '2px solid #307D7E',
        margin : 'auto',
        backgroundColor : 'lightgrey'
    };
    return(
        <div>
            <h1> input Here </h1>
            <input style={styleButton} type="text" 
            onChange={props.changed}
            value={props.currentName}/>
        </div>  
    );
}

export default userInput;
