import React from 'react';

const userOutput = (props) => {
    const style= {
        width : '30%',
        backgroundColor : '#FEFCFF',
        border : '1px solid grey',
        textAlign : 'center'
    };
    return(
        <div style={style}> 
            <h1> UserName : {props.userName} </h1>
            <p> output  will be displayed here </p>
        </div>
    );
}

export default userOutput;