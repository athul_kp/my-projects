import React from 'react';
import './App.css';
import PersistentDrawerLeft from './Components/PersistentDrawerLeft';
import {Provider} from 'react-redux'
import store from './store/index'


function App() {
  return (
    <div className="App">
     <Provider store={store}> <PersistentDrawerLeft/> </Provider>
        
    </div>
  );
}

export default App;
