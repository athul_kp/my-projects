const reducer = (state ={
  fetching : false,
    fetched :false,
    tasks:[],
    error:null,
    done: false
},action)=>{
    switch(action.type){
        case 'FETCH_PRODUCT_PENDING': {
            return { ...state, fetching: true }
          }
          case 'FETCH_PRODUCT_REJECTED': {
            return { ...state, fetching: false, error: action.payload }
          }
          case 'FETCH_PRODUCT_FULFILLED': {
            console.log("ACTION = " + JSON.stringify(action.payload))
            return {
              ...state,
              fetching: false,
              fetched: true,
              done: true,
              tasks: action.payload
            }
          }
          default : return {...state}
        }
    }   



export default reducer;