import React from "react";
import { connect } from "react-redux";

import {getData} from '../Actions/index'
import Tableindex from './Tableindex'

class TableData extends React.Component {
    componentDidMount() {
        this.props.dispatch(getData())
    }
    render() {
        const display = (this.props.done === true) ? 
        <Tableindex rows={this.props.tasks.data}/>
             : "Loadding..."
    return (<div >
                {display}
            </div>);
    }
}
const mapStateToProps = state =>{
    return{
        tasks: state.tasks,
        done: state.done
    }
}
export default connect(mapStateToProps)(TableData);