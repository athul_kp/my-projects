const apiConstants = {
  BASEURL: 'http://5e42aac6f6128600148ad800.mockapi.io/api/tasks',
  TASKAPI: 'tasks'
}
const apiUrl = `${apiConstants.BASEURL}`
export default apiUrl
