import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Buttons from '../Buttons';

const useStyles = makeStyles(theme => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: 200,
    },
  },
}));

export default function Addtodo() {
  const classes = useStyles();
  
  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div>
        <TextField
         id="standard-textarea"
         label="Name"
         placeholder="Name"
         multiline
        />
        <TextField
          id="standard-textarea"
          label="Task"
          placeholder="Task"
          multiline
        />
        <Buttons />
      </div>
    </form>
  );
}
