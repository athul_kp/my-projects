import {applyMiddleware , createStore } from "redux";
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import axios from 'axios';
import promise from "redux-promise-middleware";

const initialState ={
    fetching : false,
    fetched :false,
    users:[],
    error:null,
}
const reducer = (state ={initialState},action)=>{
    switch(action.type){
        case "FETCH_USERS_START":{
            return {...state,fetching:true}
            break;
        }
        case "FETCH_USERS_RECIEVED":{
            return {...state,fetching:false,users:action.payload}
            
            break;
        }
        case "FETCH_USERS_ERROR" :{
            return {...state,fetching:false,fetched:false,error:true}
            break;
        }
    }
    }   

const middleware =applyMiddleware(promise,thunk,createLogger)
const store = createStore(reducer,middleware);

store.dispatch({
    type:"FETCH_USERS",
    payload:axios.get("http://5e42aac6f6128600148ad800.mockapi.io/api/tasks")
})
