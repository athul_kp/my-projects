import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Addtodo from './components/addtodo/Addtodo';
import Todolist from './components/todolist/Todolist';
import './App.css';

class App extends Component {
  render() {
    return (
    <Router>
        <div>
          <div  className="todoheader">
            <h2>TODO APP</h2>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <ul className="navbar-nav mr-auto">
              <Link to={'/'} className="nav-link"> Todolist </Link>
              <Link to={'/Addtodo'} className="nav-link">Addtodo</Link>
            </ul>
            </nav>
         
          </div>
          
          <Switch>
              <Route exact path='/' component={Todolist} />
              <Route path='/Addtodo' component={Addtodo} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
