import React from 'react';
import './Detailspopup.css';
class Popup extends React.Component {
  render() {
    return (
      <div className='popup'>
        <div className='popup_inner'>
          <h1>{this.props.text}</h1>
              <p>Task id     =   {this.props.id}</p>
              <p>Task Name   =   {this.props.name}</p>
              <p>Task        =   {this.props.task}</p>
        <button className="closebtn" id={this.props.id} onClick={this.props.closePopup}>
            close </button>
        </div>
      </div>
    );
  }
}
class Detailspopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false,      
    };
  }
  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }
  render() {
    return (
      <div>
        <p onClick={()=>this.setState({showPopup:true})}>{this.props.name}</p>
          {this.state.showPopup ? 
            <Popup id={this.props.id} name={this.props.name} task={this.props.task}
              closePopup={this.togglePopup.bind(this)}>
            </Popup>
            : null
        }
      </div>
    );
  }
};
export default Detailspopup;