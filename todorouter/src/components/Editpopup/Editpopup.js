import React from 'react';
import Editmodel from '../editmodel/Editmodel';
import './Editpopup.css';
class Popup extends React.Component {
  render() {
    return (
      <div className='popup'>
        <div className='popup_inner'>
          <h1>{this.props.text}</h1>
          <Editmodel id={this.props.id} name={this.props.name} task={this.props.task} 
            closePopup={this.props.closePopup}/>
        
        {/* <button className="closebtn" id={this.props.id} onClick={this.props.closePopup}>
            close </button> */}
        </div>
      </div>
    );
  }
}
class Editpopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false,      
    };
  }
  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }
  render() {
    return (
      <div className='app'>
        <button onClick={this.togglePopup.bind(this )}>Edit</button>
          {this.state.showPopup ? 
            <Popup id={this.props.id} name={this.props.name} task={this.props.task}
              closePopup={this.togglePopup.bind(this)}>
            </Popup>
            : null
        }
      </div>
    );
  }
};
export default Editpopup;