import React, { Component } from 'react';
import './addtodo.css';
class Addtodo extends Component{
    state={
        name:'',
        task:'' 
    }   
    handleSubmit(event){
        event.preventDefault();
        alert(event.target.name.value);
        fetch('http://5e42aac6f6128600148ad800.mockapi.io/api/tasks/',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body:JSON.stringify({
                name:event.target.name.value,
                task:event.target.task.value
            })
        })
        .then(res => res.json())
        .then(this.setState({
            name:'',
            task:''
        }))
        
    } 
    render(){
        return(
                <div className="addtodo">
                <form onSubmit={this.handleSubmit}>
                <input  
                        className="addname"
                        type="text"
                        name="name"
                        required
                        placeholder="Name"
                        />
                <textarea
                        className="addtext"
                        type="text"
                        name="task"
                        required
                        placeholder="Task"      
                        />
                <div>
                     <button className="addbtn" variant="primary" type="submit"> ADDTODO    </button>
                </div>
                </form>
            </div>           
        );
    }
}
export default Addtodo;