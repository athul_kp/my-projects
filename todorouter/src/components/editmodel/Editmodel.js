import React, { Component } from 'react';
import './Editmodel.css'
 class Editmodel extends Component{
    state = {
        tasks:[],
    }
    constructor(props){
        super(props);
        this.handleSubmit=this.handleSubmit.bind(this);
    }

    async componentDidUpdate() {
        // const url = "http://5e42aac6f6128600148ad800.mockapi.io/api/tasks";
        // const response =await fetch(url);
        // const data = await response.json();
        console.log("testing")
        // this.setState({tasks:data})
        // .then(console.log(this.componentDidUpdate.data))    
        }

    handleSubmit(event){
        event.preventDefault();
        alert(event.target.name.value);
        fetch('http://5e42aac6f6128600148ad800.mockapi.io/api/tasks/'+this.props.id,{
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body:JSON.stringify({   
                key:event.target.id.value,
                name:event.target.name.value,
                task:event.target.task.value
            })
        }).then(this.props.closePopup)
        
    }
    render(){
        return(
            <div className="divedit">
                <form className="editform" onSubmit={this.handleSubmit}>
                <input  className="editname"
                        type="text"
                        name="name"
                        defaultValue={this.props.name}
                        required
                        placeholder="Name"
                        />
                <textarea 
                        className="edittask"
                        type="text"
                        name="task"
                        defaultValue={this.props.task}
                        required
                        placeholder="Task"
                        />
                <button  className="edittool" type="submit"> Update  </button>   
                </form>
            </div>
            
        );
    }
}
export default Editmodel;