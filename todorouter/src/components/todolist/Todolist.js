import React, { Component } from 'react';
import Editpopup from '../Editpopup/Editpopup';
import Detailspopup from '../Detailspopup/Detailspopup';
class Todolist extends Component{
    state = {
        tasks : [],
        open:false
    }
    async componentDidMount() {
        const url = "http://5e42aac6f6128600148ad800.mockapi.io/api/tasks";
        const response = await fetch(url);  
        const data = await response.json();
        console.log(data)
        this.setState({tasks:data})
        // document.addEventListener('mousedown', this.handleClickOutside);
        }
    async deleteHandler (delid) {
        if(window.confirm('Are you sure ..?'))
        {
            await fetch('http://5e42aac6f6128600148ad800.mockapi.io/api/tasks/'+delid,{
                method:'DELETE',
                headers:{'Content-Type':'application/json'}
                })
                .then(response =>
                    response.json()
                    .then(json => {
                      return json;
                    })
                    .then(fetch('http://5e42aac6f6128600148ad800.mockapi.io/api/tasks/')
                    .then(res => res.json())
                    .then((data) => {
                      this.setState({ tasks: data })
                    })))
        }
    }
    render(){
        return(
            <div>
                <table>
                    <tbody>
                        <tr className="tablename">
                            <td > Name </td>
                            <td> task </td> 
                            <td>Delete</td>   
                            <td>Edit</td>    
                        </tr>
                        {this.state.tasks.map((task)=>(
                            <tr key={task.id} > 
                                <td ><Detailspopup id={task.id} name={task.name} task={task.task}/></td>
                                <td>{task.task}</td>
                                <td> 
                                    <button className="delbtn" 
                                    onClick={()=>this.deleteHandler(task.id)} 
                                    variant="danger">Delete </button> </td>
                                <td>
                                    <Editpopup id={task.id} name={task.name} task={task.task}/>
                                </td>
                            </tr>
                            ))}
                    </tbody>
                </table>
            </div>
        );
    }
}  
export default Todolist;