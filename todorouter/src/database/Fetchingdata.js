import React, { Component } from 'react';
class Fetchingdata extends Component{
    state = {
        tasks : []
    }
    async componentDidMount() {
        const url = "http://5e42aac6f6128600148ad800.mockapi.io/api/tasks";
        const response = await fetch(url);
        const data = await response.json();
        console.log(data)
        this.setState({tasks:data})
        }
        render(){
            return(
                <div>
                    <table>
                        <tbody>
                            <tr className="tablename">
                                <td> Name </td>
                                <td> task </td> 
                                <td>Delete</td>   
                                <td>Edit</td>    
                            </tr>
                            {this.state.tasks.map((task)=>(
                                <tr key={task.id}> 
                                    <td>{task.name}</td>
                                    <td>{task.task}</td>
                                </tr>
                                ))}
                        </tbody>
                    </table>
                </div>
            );
        }
}

export default Fetchingdata;