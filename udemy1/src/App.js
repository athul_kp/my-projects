import React,{useState} from 'react';
import './App.css';
import Person from './Person/Person';


const App = props => {
  const [personState , setPersonState] = useState({
    persons :[
      {name:"Athul" , age:23},
      {name:"Anju" , age:23}
    ]
  });
   
  const switchNameHandler = () => {
    setPersonState({persons:[
      {name:"junu" , age:23},
      {name:"athul" , age:23}
    ]});
  };
  
  return (
    <div className="App">
      <h1> It's react app </h1>
      <button onClick={switchNameHandler}>Swith Name </button> 
      <Person name = {personState.persons[0].name} age={personState.persons[0].age} />
      <Person name ={personState.persons[1].name} age={personState.persons[1].age}> From : mangalore</ Person>
    </div>
    );
  };


export default App;
